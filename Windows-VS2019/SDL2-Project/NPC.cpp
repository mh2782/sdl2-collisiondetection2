#include "NPC.h"
#include "Animation.h"
#include "Vector2f.h"
#include "TextureUtils.h"
#include "Game.h"
#include "Player.h"
#include "AABB.h"

#include <stdexcept>
#include <string>

using std::string;

/**
 * NPC
 * 
 * Constructor, setup all the simple stuff. Set pointers to null etc. 
 *  
 */
NPC::NPC() : Sprite()
{
    state = IDLE;
    speed = 30.0f;

    maxRange = 0.4f;
    timeToTarget = 0.25f;

    targetRectangle.w = SPRITE_WIDTH;
    targetRectangle.h = SPRITE_HEIGHT;

    // Could pass these in to change the difficulty
    // of a NPC for each level etc. 
    maxRange = 0.4f;
    timeToTarget = 0.25f;
    health = 10; //MUST CHANGE BACK
    points = 10;
}

/**
 * init
 * 
 * Function to populate an animation structure from given paramters. 
 *  
 * @param renderer Target SDL_Renderer to use for optimisation.
 * @exception Throws an exception on file not found or out of memory. 
 */
void NPC::init(SDL_Renderer *renderer)
{
    //path string
    string path("assets/images/undeadking-updated.png");

    //postion
    Vector2f position(300.0f,300.0f);

    // Call sprite constructor
    Sprite::init(renderer, path, 6, &position);

    // Setup the animation structure
    animations[LEFT]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 1);
    animations[RIGHT]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 2);
    animations[UP]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 3);
    animations[DOWN]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0);
    animations[IDLE]->init(1, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0);
    animations[DEAD]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 4);

    animations[DEAD]->setLoop(false);

    for (int i = 0; i < maxAnimations; i++)
    {
        animations[i]->setMaxFrameTime(0.4f);
    }

    aabb = new AABB(this->getPosition(), SPRITE_HEIGHT, SPRITE_WIDTH);
}

void NPC::draw(SDL_Renderer* renderer) {
    Sprite::draw(renderer);
    aabb->draw(renderer);
}

/**
 * ~NPC
 * 
 * Destroys the NPC and any associated 
 * objects 
 * 
 */
NPC::~NPC()
{

}

void NPC::update(float dt)
{
    if(state != DEAD) //don't move dead sprite
        ai();
    else    
    {
        respawn();
       // velocity->setX(0.0f);
       // velocity->setY(0.0f);
    }
    
    Sprite::update(dt);
}

/**
 * ai
 * 
 * Adjusts the velocity / state of the NPC
 * to follow the player
 * 
 */
void NPC::ai()
{
    // get player
    Player* player = game->getPlayer();

    // get distance to player
        // copy the player position
    Vector2f vectorToPlayer(player->getPosition());
        // subtract our position to get a vector to the player
    vectorToPlayer.sub(position);
    float distance = vectorToPlayer.length();

    // if player 'in range' stop and fire
    if(distance < maxRange)
    {
        // put fire code in here later!
    }   
    else
    {
        // else - head for player

        // Could do with an assign in vector
        velocity->setX(vectorToPlayer.getX());
        velocity->setY(vectorToPlayer.getY());   
        
        // will work but 'wobbles'
        //velocity->scale(speed);   

        // Arrive pattern 
        velocity->scale(timeToTarget);

        if(velocity->length() > speed)
        {
            velocity->normalise();
            velocity->scale(speed);            
        }

        state = IDLE;

        if(velocity->getY() > 0.1f)
            state = DOWN;
        else
            if(velocity->getY() < -0.1f)
                state = UP;
            // empty else is not an error!
        
        if(velocity->getX() > 0.1f)
            state = RIGHT;
        else
            if(velocity->getX() < -0.1f)
                state = LEFT;
            // empty else is not an error. 
    }
}

void NPC::setGame(Game* game)
{
    this->game = game;
}

int NPC::getCurrentAnimationState()
{
    return state;
}

void NPC::takeDamage(int damage)
{
    health -= damage;

    if(!(health > 0))
        state = DEAD;
}

bool NPC::isDead()
{
    if (health > 0)
        return false;
    else 
        return true;
}

void NPC::respawn()
{
    srand(SDL_GetTicks());

    health = 10;
    state = IDLE;

    Vector2f randomPostion;
    int randSpawnWidth;
    int randSpawnHeight;

    //int extraWidth = game->WINDOW_WIDTH * 1.25f;
    //int extraHeight = game->WINDOW_HEIGHT * 1.25f;
    //printf("Game Window Width : %i\n", game->WINDOW_WIDTH);
    //printf("Game Window Height : %i\n", game->WINDOW_HEIGHT);

    /*randSpawnWidth = rand() % 200;
    randSpawnHeight = rand() % 200;
    printf("X Coord = %i\n", randSpawnHeight);
    printf("Y Coord = %i\n", randSpawnWidth);


    if (randSpawnWidth <= (game->WINDOW_WIDTH + 100)) {
        randSpawnWidth *= -1;
        randSpawnWidth -= SPRITE_WIDTH;
        printf("X Coord after x -1 = %i\n", randSpawnHeight);
    }

    if (randSpawnHeight <= (game->WINDOW_HEIGHT + 100)) {
        randSpawnHeight *= -1;
        randSpawnHeight -= SPRITE_HEIGHT;
        printf("Y Coord After x -1 = %i\n", randSpawnWidth);
    }*/

    int randX = rand() % 200;
    int randY = rand() % (game->WINDOW_HEIGHT + 200);
    randX -= 100;
    randY -= 100;

    if (randX < 0) {
        randX -= SPRITE_WIDTH;
    }
    else {
        randX += SPRITE_WIDTH;
        randX += game->WINDOW_WIDTH;
    }

    //int extraWidth = 900;
    //int extraHeight = 700;

    // get a random number between 0 and 
    // 2 x screen size. 
    //int xCoord = rand() % extraWidth;
    //printf("Width Extra = %i\n", extraWidth);
    //printf("Height Extra = %i\n", extraHeight);
    //int yCoord = rand() % extraHeight;
    
    

    //if its on screen move it off. 
    //if (randSpawnHeight > game->WINDOW_HEIGHT) {
    //   randSpawnHeight *= -1;
    //    
    //}

    //if (randSpawnWidth > game->WINDOW_WIDTH) {
    //  randSpawnWidth *= -1;
    //}d

    randomPostion.setX(randX);
    randomPostion.setY(randY);

    this->setPosition(&randomPostion);    
}
    
int NPC::getPoints()
{
    return points;
}

void NPC::setPoints(int pointValue)
{
    points = pointValue;
}
    