#include "AABB.h"
#include "Vector2f.h"

AABB::AABB()
{
    halfHeight = 0.0f;
    halfWidth = 0.0f;
    centreX = 0.0f;
    centreY = 0.0f;
}

AABB::AABB(Vector2f* position, float height, float width)
{
    // We can get away with this as the init method
    // doesn't allocate any memory!
    init(position, height, width);
}

AABB::~AABB()
{

}

void AABB::init(Vector2f* position, float height, float width)
{
    halfWidth = width / 2.0f;
    halfHeight = height / 2.0f;

    setPosition(position);
}

void AABB::draw(SDL_Renderer* renderer) {
    float x1 = centreX - halfWidth;
    float y1 = centreY - halfHeight;
    float x2 = centreX + halfWidth;
    float y2 = centreY + halfHeight;

    SDL_RenderDrawLine(renderer, x1, y1, x1, y2);
    SDL_RenderDrawLine(renderer, x2, y1, x2, y2);
    SDL_RenderDrawLine(renderer, x1, y1, x2, y1);
    SDL_RenderDrawLine(renderer, x1, y2, x2, y2);

    // int SDL_renderDrawLine(SDL_Renderer *renderer, int x1, int y1, int x2, int y2);
    /*SDL_RenderDrawLine(renderer, centreX - halfWidth, centreY - halfHeight, centreX - halfWidth, centreY + halfWidth);
    SDL_RenderDrawLine(renderer, centreX - halfWidth, centreY - halfHeight, centreX + halfWidth, centreY - halfWidth);
    SDL_RenderDrawLine(renderer, centreX + halfWidth, centreY - halfHeight, centreX + halfWidth, centreY + halfWidth);
    SDL_RenderDrawLine(renderer, centreX - halfWidth, centreY + halfHeight, centreX + halfWidth, centreY + halfWidth);*/
}

float AABB::getHalfHeight()
{
    return halfHeight;
}

float AABB::getHalfWidth()
{
    return halfWidth;
}

float AABB::getCentreX()
{
    return centreX;
}

float AABB::getCentreY()
{
    return centreY;
}

void AABB::setPosition(Vector2f* position)
{
    centreX = position->getX() + halfWidth;
    centreY = position->getY() + halfHeight;
}

bool AABB::intersects(AABB* other)
{
    // If the sprite has no collision box
    // then give up. 
    if(other == nullptr)
        return false;

    bool hCheck = (fabs((double)this->getCentreX() - other->getCentreX()) < ((double)this->getHalfWidth() + other->getHalfWidth()));
    bool vCheck = (fabs((double)this->getCentreY() - other->getCentreY()) < ((double)this->getHalfHeight() + other->getHalfHeight()));

    return hCheck && vCheck;  
}