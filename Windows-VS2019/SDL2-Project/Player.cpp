#include "Player.h"
#include "Animation.h"
#include "Vector2f.h"
#include "TextureUtils.h"
#include "Game.h"
#include "AABB.h"
#include "NPC.h"

#include <stdexcept>
#include <string>

using std::string;

// Only types with a fixed bit representaition can be
// defined in the header file
const float Player::COOLDOWN_MAX = 0.2f;

/**
 * Player
 * 
 * Constructor, setup all the simple stuff. Set pointers to null etc. 
 *  
 */
Player::Player() : Sprite()
{
    state = IDLE;
    speed = 50.0f;
    health = 100;

    targetRectangle.w = SPRITE_WIDTH;
    targetRectangle.h = SPRITE_HEIGHT;

    // Initialise weapon cooldown. 
    cooldownTimer = 0;

    // Init points
    points = 0;
}

/**
 * initPlayer
 * 
 * Function to populate an animation structure from given paramters. 
 *  
 * @param renderer Target SDL_Renderer to use for optimisation.
 * @exception Throws an exception on file not found or out of memory. 
 */
void Player::init(SDL_Renderer *renderer)
{
    //path string
    string path("assets/images/rpg_sprite_walk.png");

    //postion
    Vector2f position(200.0f,200.0f);

    // Call sprite constructor
    Sprite::init(renderer, path, 5, &position);

    // Setup the animation structure
    animations[LEFT]->init(8, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 2);
    animations[RIGHT]->init(8, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 3);
    animations[UP]->init(8, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 1);
    animations[DOWN]->init(8, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0);
    animations[IDLE]->init(1, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0);

    for (int i = 0; i < maxAnimations; i++)
    {
        animations[i]->setMaxFrameTime(0.2f);
    }

    aabb = new AABB(this->getPosition(), SPRITE_HEIGHT, SPRITE_WIDTH);

}

/**
 * ~Player
 * 
 * Destroys the player and any associated 
 * objects 
 * 
 */
Player::~Player()
{

}

/**
 * processInput
 * 
 * Method to process inputs for the player 
 * Note: Need to think about other forms of input!
 * 
 * @param keyStates The keystates array. 
 */
void Player::processInput(const Uint8 *keyStates)
{
    // Process Player Input

    //Input - keys/joysticks?
    float verticalInput = 0.0f;
    float horizontalInput = 0.0f;

    // If no keys are down player should not animate!
    state = IDLE;

    // This could be more complex, e.g. increasing the vertical
    // input while the key is held down.
    if (keyStates[SDL_SCANCODE_UP])
    {
        verticalInput = -1.0f;
        state = UP;
    }

    if (keyStates[SDL_SCANCODE_DOWN])
    {
        verticalInput = 1.0f;
        state = DOWN;
    }

    if (keyStates[SDL_SCANCODE_RIGHT])
    {
        horizontalInput = 1.0f;
        state = RIGHT;
    }

    if (keyStates[SDL_SCANCODE_LEFT])
    {
        horizontalInput = -1.0f;
        state = LEFT;
    }

    // Calculate player velocity.
    velocity->setX(horizontalInput);
    velocity->setY(verticalInput);
    velocity->normalise();
    velocity->scale(speed);

    if (keyStates[SDL_SCANCODE_SPACE])
    {
        fire();
    }
}

int Player::getCurrentAnimationState()
{
    return state;
}

void Player::setGame(Game* game)
{
    this->game = game;
}

void Player::update(float timeDeltaInSeconds)
{
    Sprite::update(timeDeltaInSeconds);

    cooldownTimer += timeDeltaInSeconds;

}

void Player::fire()
{   
    // Need a cooldown timer, otherwise we shoot 
    // a million bullets a second and our npc
    // dies instantly
    if(cooldownTimer > COOLDOWN_MAX)
    {
        // Can't fire in idle state!
        if(velocity->length() > 0.0f)
        {
            game->createBullet(position,velocity);
            cooldownTimer = 0;
        }
    }   
}

void Player::addScore(int points)
{
    this->points += points;    
}

int Player::getScore()
{
    return points;
}

void Player::respawn()
{
    health = 10;
    state = IDLE;

    Vector2f randomPostion;
    int randSpawnWidth;
    int randSpawnHeight;

    //int extraWidth = game->WINDOW_WIDTH * 1.25f;
    //int extraHeight = game->WINDOW_HEIGHT * 1.25f;
    //printf("Game Window Width : %i\n", game->WINDOW_WIDTH);
    //printf("Game Window Height : %i\n", game->WINDOW_HEIGHT);

    /*randSpawnWidth = rand() % 200;
    randSpawnHeight = rand() % 200;
    printf("X Coord = %i\n", randSpawnHeight);
    printf("Y Coord = %i\n", randSpawnWidth);


    if (randSpawnWidth <= (game->WINDOW_WIDTH + 100)) {
        randSpawnWidth *= -1;
        randSpawnWidth -= SPRITE_WIDTH;
        printf("X Coord after x -1 = %i\n", randSpawnHeight);
    }

    if (randSpawnHeight <= (game->WINDOW_HEIGHT + 100)) {
        randSpawnHeight *= -1;
        randSpawnHeight -= SPRITE_HEIGHT;
        printf("Y Coord After x -1 = %i\n", randSpawnWidth);
    }*/

    int randX = rand() % 200;
    int randY = rand() % (game->WINDOW_HEIGHT + 200);
    randX -= 100;
    randY -= 100;

    if (randX < 0) {
        randX -= SPRITE_WIDTH;
    }
    else {
        randX += SPRITE_WIDTH;
        randX += game->WINDOW_WIDTH;
    }



    //int extraWidth = 900;
    //int extraHeight = 700;

    // get a random number between 0 and 
    // 2 x screen size. 
    //int xCoord = rand() % extraWidth;
    //printf("Width Extra = %i\n", extraWidth);
    //printf("Height Extra = %i\n", extraHeight);
    //int yCoord = rand() % extraHeight;



    //if its on screen move it off. 
    //if (randSpawnHeight > game->WINDOW_HEIGHT) {
    //   randSpawnHeight *= -1;
    //    
    //}

    //if (randSpawnWidth > game->WINDOW_WIDTH) {
    //  randSpawnWidth *= -1;
    //}d

    randomPostion.setX(randX);
    randomPostion.setY(randY);

    this->setPosition(&randomPostion);
}